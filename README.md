# OpenML dataset: Indian-Liver-Patient-Patient-Records-KFolds-5folds

https://www.openml.org/d/43403

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Liver disease are in India are increasing due to excessive consumption of alcohol and other harmful substaces present in air or food items or drugs. This dataset is created for predictive analysis of the liver disease to reduce the burden on the doctors.
Content
This data set contains 416 liver patient records and 167 non liver patient records collected from North East of Andhra Pradesh, India. The "Dataset" column is a class label used to divide groups into liver patient (liver disease) or not (no disease). This data set contains 441 male patient records and 142 female patient records. This dataset has already been preprocessed by filling the missing values with average value of the respective columns. Also the data has been divided into 5 KFolds by doing stratified KFolds as the data was imbalanced.
Columns:
Age of the patient
Gender of the patient
Total Bilirubin
Direct Bilirubin
Alkaline Phosphotase
Alamine Aminotransferase
Aspartate Aminotransferase
Total Protiens
Albumin
Albumin and Globulin Ratio
Target:  1 - Patient,  0 - Not a patient
Acknowledgements
This dataset was downloaded from UCI ML Repository.
Inspiration
For inspiration of this dataset came for predicting the liver disease to lessen the burden of the doctors.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43403) of an [OpenML dataset](https://www.openml.org/d/43403). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43403/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43403/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43403/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

